import processing.serial.*;

Serial myPort;
String val; // Datos recibidos desde el puerto serie
char letter;
String words = "Distancia: ";

void setup() {
  size(640, 360);

  // Asegúrate de seleccionar el puerto correcto
  String portName = Serial.list()[0]; 
  myPort = new Serial(this, portName, 9600);
  background(0); // Fondo negro inicial
}

void draw() {
  background(0); // Refrescar fondo

  textSize(14);
  text("La distancia medida por el sensor es:", 50, 50);

  textSize(36);
  text(words, 50, 120, 540, 300);
}

void serialEvent(Serial myPort) {
  String val = myPort.readStringUntil('\n');

  if (val != null) {
    val = val.trim();
    words = "Distancia: " + val + " cm";
  }
}
